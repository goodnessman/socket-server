var app = require('express')();
var bodyParser        = require('body-parser');
var server = require('http').Server(app);
var io = require('socket.io')(server);
var port = process.env.PORT || 3000;

var mysqlModule = require("mysql");
// var mysql = mysqlModule.createConnection({
// 	"host" 		: "",
// 	"user" 		: "root",
// 	"password" 	: "",
// 	"database"  : "mashina_steam"
// });

var mysql = mysqlModule.createConnection({
	"host" 		: "mashina.mysql.ukraine.com.ua",
	"user" 		: "mashina_steam",
	"password" 	: "e23yapv2",
	"database"  : "mashina_steam"
});

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

server.listen(port);

app.get('/', function (req, res) {
  res.end('Server is running...');
});

// отправляет задание для формирования ссылки подтверждения трейда
app.post('/createOfferUrl', function (req, res) {
	var data = req.body;
	console.log(data);

	console.log('запрос на создание ссылки:');
	console.log(data);

	if (checkGetData(data)) {
		io.emit('createOfferUrlTask', data);//событие клиента на Angular
	}

  	res.end(JSON.stringify({'success':'true', 'data': data}));
});

// отправляет задание для добавления нового елемента у пользователя
app.post('/addNewTradeItem', function (req, res) {
	var data = req.body;

	console.log('нотификация от системы: ');
	console.log(data);

	io.emit('addNewTradeItemTask', data);

  	res.end(JSON.stringify({'success':'true'}));
});

console.log('Server is running on port '+port+'...');

io.on("connection", function(socket){

	// новы елемент продан на сайте
	socket.on('newItemTradeOnSite', function (data) {
	  // console.log('newItemTradeOnSite');

	  io.emit('newItemTradeOnSite', data);
	});

	socket.on('noteFromBot', function (data) {
	  console.log('нотификация от бота: '+data);

	  io.emit('notification', data);
	});

	socket.on('userConnected', function (data) {

	  mysql.query("INSERT INTO user_online (steamid, page) VALUES (?, ?)", [data.user_id, socket.id], function(err){
	  	if (err) console.log(err);

	  	// console.log('соединение "'+socket.id+'" пользователя "'+data.user_id+'" зарегистрировано успешно');
	  });	

	});

	socket.on("disconnect", function () {
        setTimeout(function () {
            
            console.log('пользователь "'+socket.id+'" ofline');

            mysql.query("DELETE FROM user_online WHERE page='"+socket.id+"'", function(err){
            	if (err) console.log(err);
            	
            	// console.log('отсоединение "'+socket.id+'" зарегистрировано успешно');
            });


        }, 500);                        
    });

	// бан пользователей
	socket.on('bannedUser', function (data) {

	  io.emit('checkBan', data);
	});


	// удалить сообщение пользователя
	socket.on('deleteThisChatMessage', function (data) {
		console.log('удалить сообщение "'+data.msg+'" из чата у пользователя "'+data.name+'" - "'+data.user_id+'"');

		mysql.query("DELETE FROM user_chat_msg WHERE steamid='"+data.user_id+"' AND msg='"+data.msg+"'", function(err){
			if (err) console.log(err);
			
			io.emit('deleteChatMessage', data);

			console.log("сообщение удалено успешно");
		});
	  
	});

	socket.on("ChatMessage", function(data){
		console.log('новое сообщение сообщение "'+data.msg+'" от пользователя "'+data.name+'"');

		mysql.query("INSERT INTO user_chat_msg (steamid, msg) VALUES (?, ?)", [data.steamid, data.msg], function(err){
			if (err) console.log(err);
			
			io.emit("MessageAddedSuccess", data);

			console.log("новое сообщение добавлено успешно");
		});
	});

	mysql.query("SELECT user_chat .steamid, user_chat .is_ban, user_chat .name, user_chat_msg .msg, user_chat_msg .id FROM user_chat JOIN user_chat_msg on user_chat.steamid = user_chat_msg.steamid order by user_chat_msg.id  ", function(err, rows){
		if (err) console.log(err);

		io.emit("Refresh", rows);
	});	

});

// heck data get from rost request
function checkGetData(data) {

    if (!data.offer_id || !(data.offer_id.length > 0)) {
        
       	console.log('"offer_id" valid data');
        return false;
    }

    if (!data.user_id || !(data.user_id.length > 0)) {
        
       	console.log('"user_id" valid data');
        return false;
    }

    return true;
}